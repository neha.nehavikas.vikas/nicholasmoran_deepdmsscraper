# https://www.upwork.com/freelancers/vikasojha
import requests


class Browser:
    def __init__(self):
        print('Preparing Session...')
        self.s = requests.session()
        self.s.headers.update({
            'User-Agent': (
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) '
                'Chrome/105.0.0.0 Safari/537.36'
            )
        })

    def login(self, username, password):
        print('Logging In...')
        url = 'https://depedms.dep.state.fl.us/Oculus/servlet/login'
        self.s.get(url)

        params = {'action': 'login'}
        data = {
            'CLEAR_ERROR_MSG': 'N',
            'changePassword': '',
            'formUser': username,
            'formPassword': password
        }
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        r = self.s.post(url, headers=headers, params=params, data=data)
        if 'loginInvalidPassword' in r.url:
            print('Login Failed!')
            return False
        else:
            print('Login Successful!')
            return True

    def search(self):
        print('Searching Records...')

        url = 'https://depedms.dep.state.fl.us/Oculus/servlet/search'
        self.s.get(url)
        self.s.cookies.update({'background': 'Original'})
        params = {
            'action': 'property',
            'screen': 'search',
            'CatalogIDs': '75',
            'tabindex': '3',
            'profiles': 'AIR File Cabinet',
            'searchByType': 'Profile'
        }
        data = {
            'catalog2': '75',
            'propertyNah': '',
            'catalog': '75',
            'searchByType': 'Profile',
            'profileProperty': 'AIR File Cabinet',
            'profile': 'AIR File Cabinet',
            'property': ''
        }
        self.s.post(url, params=params, data=data)

        url = 'https://depedms.dep.state.fl.us/Oculus/servlet/hitlist'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        params = {'action': 'hitlist', 'searchResultParam': 'fromSearchResultPage'}
        data = {
            'catalog2': '75',
            'propertyNah': '',
            'catalog': '75',
            'searchByType': 'Profile',
            'profileProperty': 'AIR File Cabinet',
            'profile': 'AIR File Cabinet',
            'property': '',
            'SortBy': 'Document Date',
            'type': 'any',
            'dmsParameterCreator': '',
            'attrFolderName': '',
            'dmsParameterCreateDate': '',
            'dmsParameterCreateDate_TO_': '',
            '_OP_Drawer': '=',
            '_PARA_Drawer': 'COMPLAINT RECORDS',
            '_OP_Folder': '=',
            '_PARA_Folder': '',
            '_OP_Tab': '=',
            '_PARA_Tab': '',
            '_OP_Document Date': '[...',
            '_PARA_Document Date': '',
            'Document Date_TO_': '',
            '_OP_Document Title': '=',
            '_PARA_Document Title': '',
            '_OP_Document Subject': '=',
            '_PARA_Document Subject': ''
        }
        self.s.post(url, headers=headers, params=params, data=data)

        print('Downloading Excel...')
        url = 'https://depedms.dep.state.fl.us/Oculus/servlet/hitlist'
        params = {
            'action': 'hitlistrefresh',
            'PageSize': 'All',
            'SelectedCatalogGuid': '75',
            'profile': 'AIR File Cabinet',
            'excelhitlist': 'yes'
        }
        r = self.s.get(url, params=params)
        with open('results.xlsx', 'wb') as f:
            f.write(r.content)


if __name__ == '__main__':
    browser = Browser()
    if browser.login('netuser', 'netuser'):
        browser.search()
